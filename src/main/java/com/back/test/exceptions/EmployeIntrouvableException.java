package com.back.test.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EmployeIntrouvableException extends Throwable {
	
public	EmployeIntrouvableException(String s) {
	super(s);
}
}
