package com.back.test.model;




import javax.persistence.*;


@Entity
@Table(name="Departement")
public class Departement {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id_departement;
    
    @Column(name="nom_departement",nullable = false)
    private String nom;

    
 
    
    
    
    

    public Departement(Integer id_departement, String nom_departement) {
	
		this.id_departement = id_departement;
		this.nom = nom_departement;
	
	}

	public Departement(){
        super();
    }

    public Integer getId_departement() {
		return id_departement;
	}

	public void setId_departement(Integer id_departement) {
		this.id_departement = id_departement;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom_departement) {
		this.nom = nom_departement;
	}

	
	

	
   

    
}
