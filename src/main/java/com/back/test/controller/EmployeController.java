package com.back.test.controller;
import com.back.test.dao.EmployeRepository;
import com.back.test.model.Employe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.transaction.Transactional;
import javax.validation.Valid;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Transactional
@RestController
@RequestMapping(value = "api/employe")
@CrossOrigin(origins = "*")
public class EmployeController {

    @Autowired
    EmployeRepository employeRepository;
    @GetMapping(value = "/{id}")
    public Employe getEmploye(@PathVariable Long id ) throws Exception{
        return employeRepository.findById(id).orElseThrow(()->new Exception("!!!!!"));
    }
    @GetMapping(value = "/all")
    public List<Employe> getAll(){
        return employeRepository.findAll();
    }
    @DeleteMapping(value="/{id}")
    public Map<String, Boolean> deleteEmploye(@PathVariable Long id) throws Exception{
    	Employe employe = employeRepository.findById(id).orElseThrow(()->new Exception("Employe not found"));
        employeRepository.delete(employe);
        Map<String,Boolean> response = new HashMap<>();
        response.put("Deleted",Boolean.TRUE);
        return response;
    }
    @PutMapping(value="/{id}")
    public ResponseEntity<Employe> updateStatus (@PathVariable Long id) throws Exception{
    	Employe employe = employeRepository.findById(id).orElseThrow(()->new Exception("Employe not found"));
    	employe.setPassword(employe.getPassword());
     
    	Employe updated = employeRepository.save(employe);
        return ResponseEntity.ok(updated);
    }

    @PostMapping(value="/addUser")
    public Employe addUser(@Valid @RequestBody Employe employe){
        return employeRepository.save(employe);
}
}
