package com.back.test.controller;
import com.back.test.dao.DepartementRepository;
import com.back.test.model.Departement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value="/rest/api")
@CrossOrigin(origins = "http://localhost:4200")
public class DepartementController {
    @Autowired
    DepartementRepository departementRepository;
    @GetMapping(value="/allDepartement")
    public List<Departement> allDepartement(){
        return departementRepository.findAll();
    }
    @PostMapping(value="/addDepartement")
    public Departement addDepartement(@Valid @RequestBody Departement c){
        return departementRepository.save(c);
    }
    @GetMapping(value="/departement/{id}")
    public ResponseEntity<Departement> getDepartement(@PathVariable Integer id) throws Exception{
    	Departement c = departementRepository.findById(id).orElseThrow(()->new Exception("Le Departement n'existe pas"));
        return ResponseEntity.ok().body(c);
    }
    @PutMapping(value="/departement/{id}")
        public ResponseEntity<Departement> updateDepartement(@PathVariable Integer id,@Valid @RequestBody Departement DepartementDetails) throws Exception{
    	Departement c = departementRepository.findById(id).orElseThrow(()->new Exception("La catégorie n'existe pas"));
        c.setNom(DepartementDetails.getNom());
        Departement updateDepartement = departementRepository.save(c);
        return ResponseEntity.ok(updateDepartement);
    }

    @DeleteMapping(value="/departement/{id}")
    
    public Map<String,Boolean> deleteDepartement(@PathVariable Integer id) throws Exception {
    	Departement departement = departementRepository.findById(id).orElseThrow(()->new Exception("Le departement n'existe pas"));
        departementRepository.delete(departement);
        Map<String,Boolean> response = new HashMap<>();
        response.put("departement est supprimé!",Boolean.TRUE);
        return response;
    }
}
