package com.back.test.dao;

import java.util.Optional;

import com.back.test.model.Employe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
 
@Repository
public interface EmployeRepository extends JpaRepository<Employe, Long> {
    Optional<Employe> findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
}