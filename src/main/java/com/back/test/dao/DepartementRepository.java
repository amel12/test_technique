package com.back.test.dao;

import com.back.test.model.Departement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;


@EnableJpaRepositories("com.formalab.ecommerce.dao")
public interface DepartementRepository extends JpaRepository<Departement,Integer> {

   /* @Transactional
    @Modifying
    @Query("UPDATE Departement p SET p.nom_departement =:nom WHERE p.id_departement=:id")
    public void updateDepartement(@Param("id") Integer id,@Param("nom") String nom);*/
}
