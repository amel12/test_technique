package com.back.test.dao;

import java.util.Optional;

import com.back.test.model.Role;
import com.back.test.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;
 
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}